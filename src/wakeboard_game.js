// WakeboardGame module
(function (root) {
	var WakeboardGame = root.WakeboardGame = ( root.WakeboardGame || {})

	var Boat = WakeboardGame.Boat = function (sponsor) {
		this.sponsor = sponsor;
		this.power = function () { return 'power'; };
		this.turn = function () {return 'turn'; };
    this.sink = function () {return 'sink'; }
	};

	var Wakeboarder = WakeboardGame.Wakeboarder = function (name, sponsor) {
		this.name = name;
		this.sponsor = sponsor;
		this.jump = function () { return 'jump' };
		this.spin = function () { return 'spin' };
		this.grind = function () { return 'grind' };
		this.crash = function () {return 'crash' };
	};


})(this);