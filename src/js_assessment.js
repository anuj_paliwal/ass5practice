(function(root){
	var Assessment = root.Assessment = (root.Assessment || {});

	var merge = Assessment.merge = function(arr1, arr2) {
		var sorted = [];
		while (arr1.length !== 0 && arr2.length !== 0) {
			if (arr1[0] <= arr2[0]) {
				sorted.push(arr1.shift());
			} else {
				sorted.push(arr2.shift());
			}
		}


		arr1.length === 0 ? sorted = sorted.concat(arr2) : sorted = sorted.concat(arr1);
		return sorted;
	};

	var mergeSort = Assessment.mergeSort = function(array) {
		if (array.length < 2) {
			return array;
		} else {
			var middle = Math.floor(array.length/2);
			var left = mergeSort(array.slice(0, middle));
			var right = mergeSort(array.slice(middle, array.length));
			return merge(left, right);
		}
	};

	var recursiveExponent = Assessment.recursiveExponent = function(base, power) {
		if (power === 0) {
			return 1;
		}
		else {
			return base * recursiveExponent(base, power - 1);
		}
	};

	var powCall = Assessment.powCall = function (base, power, callback) {

		var meh = recursiveExponent(base, power);
		callback(Math.pow(base, power));
	};

	var transpose = Assessment.transpose = function (matrix) {
		var transposed = [];
		for (var i = 0; i < matrix.length; i++) {
			transposed.push([]);
		}
		for (var i = 0; i < matrix.length; i++) {
			for (var j = 0; j < matrix[i].length; j++) {
				transposed[i].push(matrix[j][i]);
			}
		}

		return transposed;
	};

	var primes = Assessment.primes = function (n) {
		if (n === 0) { return [] }
		var pNums = [2];
		var checkNum = 3;

		while (pNums.length < n) {
			var prime = true;
			pNums.forEach(function(pNum) {
				if (checkNum % pNum === 0) { prime = false;}
			});
			if (prime) {pNums.push(checkNum)}
			checkNum += 2;
		}

		return pNums;
	};

	Function.prototype.myCall = function (context, args) {

		//return this.apply(context, args);
	};

	Function.prototype.inherits = function (Parent) {
		var Surrogate = function() {};
		Surrogate.prototype = Parent.prototype;
		this.prototype = new Surrogate;
	};






})(this);
